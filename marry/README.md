Original cog written by PlanetTeamSpeakk/PTSCogs https://github.com/PlanetTeamSpeakk/PTSCogs

Rewritten, modified and improved by me, dimxxz

Commands:

```
[p]marry [user]
[p]marryit [thing]			*for people that want to marry non-users
[p]forcemarry [user1] [user2]
[p]divorce [ID]
[p]massdivorce
[p]setmarrylimit			*default is set to 1
[p]marrylimit
[p]marriagetoggle
[p]marrycount
```

Replace `[p]` with your bot's prefix.


Installation:

Replace `[p]` with your bot's prefix.
```
[p]cog repo add dimxxz-Cogs https://github.com/dimxxz/dimxxz-Cogs
```
```
[p]cog install dimxxz-Cogs marry
```
